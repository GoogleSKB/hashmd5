﻿namespace HashMD5
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblPassword = new System.Windows.Forms.Label();
            this.lblHash = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.lblHashResult = new System.Windows.Forms.Label();
            this.btnDohash = new System.Windows.Forms.Button();
            this.btnCopyHash = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Location = new System.Drawing.Point(12, 12);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(56, 13);
            this.lblPassword.TabIndex = 0;
            this.lblPassword.Text = "Password:";
            // 
            // lblHash
            // 
            this.lblHash.AutoSize = true;
            this.lblHash.Location = new System.Drawing.Point(12, 41);
            this.lblHash.Name = "lblHash";
            this.lblHash.Size = new System.Drawing.Size(35, 13);
            this.lblHash.TabIndex = 1;
            this.lblHash.Text = "Hash:";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(74, 9);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(309, 20);
            this.txtPassword.TabIndex = 2;
            // 
            // lblHashResult
            // 
            this.lblHashResult.AutoSize = true;
            this.lblHashResult.Location = new System.Drawing.Point(71, 41);
            this.lblHashResult.Name = "lblHashResult";
            this.lblHashResult.Size = new System.Drawing.Size(62, 13);
            this.lblHashResult.TabIndex = 3;
            this.lblHashResult.Text = "HashResult";
            // 
            // btnDohash
            // 
            this.btnDohash.Location = new System.Drawing.Point(389, 7);
            this.btnDohash.Name = "btnDohash";
            this.btnDohash.Size = new System.Drawing.Size(130, 23);
            this.btnDohash.TabIndex = 4;
            this.btnDohash.Text = "Hash Password";
            this.btnDohash.UseVisualStyleBackColor = true;
            this.btnDohash.Click += new System.EventHandler(this.btnDohash_Click);
            // 
            // btnCopyHash
            // 
            this.btnCopyHash.Location = new System.Drawing.Point(389, 36);
            this.btnCopyHash.Name = "btnCopyHash";
            this.btnCopyHash.Size = new System.Drawing.Size(130, 23);
            this.btnCopyHash.TabIndex = 5;
            this.btnCopyHash.Text = "Copy Hash to Clipboard";
            this.btnCopyHash.UseVisualStyleBackColor = true;
            this.btnCopyHash.Click += new System.EventHandler(this.btnCopyHash_Click);
            // 
            // frmMain
            // 
            this.AcceptButton = this.btnDohash;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(535, 63);
            this.Controls.Add(this.btnCopyHash);
            this.Controls.Add(this.btnDohash);
            this.Controls.Add(this.lblHashResult);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.lblHash);
            this.Controls.Add(this.lblPassword);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "HashMD5";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.Label lblHash;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label lblHashResult;
        private System.Windows.Forms.Button btnDohash;
        private System.Windows.Forms.Button btnCopyHash;
    }
}

