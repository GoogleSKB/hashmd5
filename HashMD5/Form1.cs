﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Security.Cryptography;

namespace HashMD5
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            lblHashResult.Text = "";
        }

        private void btnCopyHash_Click(object sender, EventArgs e)
        {
            Clipboard.Clear();
            Clipboard.SetText(lblHashResult.Text);
        }

        private void btnDohash_Click(object sender, EventArgs e)
        {
            lblHashResult.Text = StringtoMD5(txtPassword.Text);
        }

        private string StringtoMD5(string input)
        {
            // step 1, calculate MD5 hash from input
            MD5 md5 = MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }

            return sb.ToString();
        }//StringtoMD5

    }
}
